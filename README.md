[![Codacy Badge](https://app.codacy.com/project/badge/Grade/ada058b48dc9412eb3ef15b6399ae6ca)](https://www.codacy.com/gl/colasri/bokeh-select/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=colasri/bokeh-select&amp;utm_campaign=Badge_Grade)

# Bokeh: point selection to notebook

The file `select.ipynb` is an example of interactive plot with the
[Bohek Visualization Library](https://bokeh.org/) inside a [Jupyter notebook](https://jupyter.org/)
environment.
After making a scatter plot (1st notebook cell), the user can select points with the mouse and
retrieve the selected points in the 2nd notebook cell for further inspection.

## Install

We'll set everything up in a brand new [Conda](https://conda.io) environment.
First, install Conda if not installed yet.
We'll also install Firefox (in case no browser is available) to run the notebook, and start the notebook inside it.

```bash
conda create -n bok -c conda-forge -y python=3 jupyter bokeh firefox pandas
conda activate bok
jupyter notebook --browser=firefox
```

## Scripts

There are two scripts using two different methods to get the selected points:

-   `select_server.ipynb` uses a Bokeh server, which creates a document which is served to the
    browser ([documentation](https://docs.bokeh.org/en/latest/docs/user_guide/server.html)).
    Example derived from [this stack overflow answer](https://stackoverflow.com/a/61482210/6352677).
    -   Upsides: The application code is 100% python.
    -   Downsides: Needs this extra "server" layer (but embedding the Bokeh server in the notebook is 
        straightforward).
-   `select_callback.ipynb` uses a `CustomJS` callback. Example derived from
    [this stack overflow answer](https://stackoverflow.com/a/61446083/6352677).
    -   Upsides: No server needed.
    -   Downsides: The application code contains more obscure IPython code and some JavaScript.

## Notes

-   Installing all the Conda stuff at once is good for:
    -   Speed, resolving and installing only once
    -   Better package compatibility
